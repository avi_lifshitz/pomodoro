# -*- coding: utf-8 -*-

import os
import gtk
import gobject

IMAGE_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'images/')
WORK_ICON = os.path.join(IMAGE_DIR, 'pomodoro.png')
REST_ICON = os.path.join(IMAGE_DIR, 'gpomodoro.png')

__all__ = ['PomodoroTimer']

class Timer(object):

    timer_counter = 0

    def __init__(self, round_time=1500):
        self.round_time = round_time
        self.time_left = self.round_time

        gobject.timeout_add_seconds(1, self.update)
        self.running = False

    def start(self):
        self.running = True

    def stop(self):
        self.running = False

    def update(self):
        if self.running:
            if self.time_left:
                self.time_left -= 1
            else:
                self.time_left = self.round_time

        return True


class PomodoroTimer(object):

    timer_counter = 0

    def __init__(self, timer):
        self.timer = timer
        self.icon = gtk.StatusIcon()
        self.icon.set_from_file(WORK_ICON)
        self.icon.set_visible(True)

        self.create_menu()

        self.start_timer()

        gobject.timeout_add_seconds(1, self.update_timer)

    def create_menu(self):
        self.menu = gtk.Menu()
        self.menu.set_title('Pomodoro')

        self.item = gtk.MenuItem()
        self.pause_item = gtk.ImageMenuItem(gtk.STOCK_MEDIA_PAUSE)
        self.pause_item.connect('activate', self.pause_timer)

        self.start_item = gtk.ImageMenuItem(gtk.STOCK_MEDIA_PLAY)
        self.start_item.connect('activate', self.start_timer)

        self.quit_item = gtk.ImageMenuItem(gtk.STOCK_QUIT)
        self.quit_item.connect('activate', gtk.main_quit, gtk)

        self.menu.append(self.item)
        self.menu.append(self.pause_item)
        self.menu.append(self.start_item)
        self.menu.append(self.quit_item)

        self.icon.connect('popup-menu', self.show_menu, self.menu)

    def show_menu(self, widget, button, time, data):
        data.show_all()
        data.popup(None, None, None, button, time)

    def pause_timer(self, widget=None):
        self.timer.stop()

    def start_timer(self, widget=None):
        self.timer.start()

    def time_is_up(self):
        self.dialog = gtk.Dialog()
        self.dialog.set_default_size = (200, 200)
        self.dialog.set_keep_above(True)
        button = gtk.Button(stock=gtk.STOCK_OK)
        button.connect('clicked', self.reset_timer)
        if self.timer.round_time == 1500:
            label = gtk.Label('Time to have a break!')
        else:
            label = gtk.Label('Get back to work!')
        self.dialog.vbox.pack_start(label)
        self.dialog.vbox.pack_start(button)
        self.dialog.show_all()
        response = self.dialog.run()
        self.dialog.destroy()

    def update_timer(self):
        if self.timer.time_left:
            time_str = '%02d:%02d' % (
                (self.timer.time_left / 60),
                (self.timer.time_left % 60),
            )
            self.item.set_label(time_str)
        else:
            self.pause_timer()
            self.time_is_up()
            self.start_timer()

        return True

    def reset_timer(self, widget=None):
        if self.timer.round_time == 1500:
            if self.timer_counter == 2:
                self.timer.round_time = 900
                self.timer_counter = 0
            else:
                self.timer.round_time = 300
                self.timer_counter += 1
            self.icon.set_from_file(REST_ICON)
        else:
            self.timer.round_time = 1500
            self.icon.set_from_file(WORK_ICON)

        self.timer.time_left = self.timer.round_time
        self.start_timer()
        self.dialog.destroy()
        print self.timer_counter


if __name__ == '__main__':
        timer = Timer()
        pomodoro = PomodoroTimer(timer)
        gtk.mainloop()
